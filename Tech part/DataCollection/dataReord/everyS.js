// Ce script est lancé sur chaque nouvel onglet il ajoute envoie toute les 100ms les datas des dernières 100ms au script background

function startRecord() {
  console.log("start");

  var clickNumber = 0;
  var keyPressed = 0;
  var scrollNumber = 0;
  var distance = 0;
  var xTravelled = 0,
    yTravelled = 0,
    prevX,
    prevY;

  $(document).on("click", function(event) {
    clickNumber++;
  });

  $(document).on("keydown", function(event) {
    keyPressed++;
  });

  $(document).on("scroll", function(event) {
    scrollNumber++;
  });

  $(document).on("mousemove", function(event) {
    prevY && (yTravelled += Math.abs(event.pageY - prevY));
    prevX && (xTravelled += Math.abs(event.pageX - prevX));

    prevX = event.pageX;
    prevY = event.pageY;

    distance = yTravelled + xTravelled;
  });

  // Toute les 100ms on charge le tableau à envoyer, on l'envoie et on le remet à 0

  setInterval(() => {
    tempArray = [clickNumber, keyPressed, scrollNumber, distance];
    clickNumber = 0;
    keyPressed = 0;
    distance = 0;
    scrollNumber = 0;
    // Message envoyé pour background.js
    chrome.runtime.sendMessage({
      type: "startBackgroundRecord",
      datas: tempArray
    });
  }, 50);
  // Message envoyé pour background.js
  chrome.runtime.sendMessage({ type: "startSaveTimeOut" });
}


//à la reception du message envoyé dans popup.js on lance la fonction de ce fichier
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.msg == "startRecord") startRecord();
  });

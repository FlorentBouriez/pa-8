//le script background est activé tout le temps il ne dépend d'aucun onglet

var finalArray = [];
finalArray.push(["clickNumber", "keyPressed", "scrollNumber", "distance"]);



function startSaveTimeOut() {
    console.log("go");
  setTimeout(() => {
    let csvContent = "data:text/csv;charset=utf-8,";
    finalArray.forEach(function(rowArray) {
      let row = rowArray.join(",");
      csvContent += row + "\r\n";
    });

    var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "data_record.csv");
    document.body.appendChild(link); // Required for FF

    link.click(); // This will download the data file named "my_data.csv".
    
  }, 5000);
}

//toute les 100ms on reçoit un tableau qu'on ajoute à final array
chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
    switch (message.type) {
      case "startBackgroundRecord":
        finalArray.push(message.datas);
        console.log(finalArray);
        
        break;
      default:
        console.error("Unrecognised message: ", message);
    }
  });

// Message envoyé par everyS.js
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.type == "startSaveTimeOut") startSaveTimeOut();
  });

import pickle
import numpy as np
import json

filename = 'finalized_model.sav'
loaded_model = pickle.load(open(filename, 'rb'))

def uman_api(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json(silent=True)
    request_args = request.args

    if request_args and 'data' in request_args:
        if 'clicks_nb' not in request_args['data']:
            return f'clicks_nb parameter missing.'
        elif 'distance' not in request_args['data']:
            return f'distance parameter missing.'
        elif 'keyPressed_nb' not in request_args['data']:
            return f'keyPressed_nb parameter missing.'
        elif 'session_time' not in request_args['data']:
            return f'session_time parameter missing.'
        else:
            return json.dumps({"result": analyse(request_args['data'])})
    elif request_json and 'data' in request_json:
        if 'clicks_nb' not in request_json['data']:
            return f'clicks_nb parameter missing.'
        elif 'distance' not in request_json['data']:
            return f'distance parameter missing.'
        elif 'keyPressed_nb' not in request_json['data']:
            return f'keyPressed_nb parameter missing.'
        elif 'session_time' not in request_json['data']:
            return f'session_time parameter missing.'
        else:
            return json.dumps({"result": analyse(request_json['data'])})
    else:
        return f'Error: data is missing. Exemple: ' + json.dumps({"data": { "clicks_nb": 10, "distance": 10, "keyPressed_nb": 10, "session_time": 10 }})

def analyse(data):
    clicks_nb = data['clicks_nb']
    distance = data['distance']
    keyPressed_nb = data['keyPressed_nb']
    session_time = data['session_time']

    # clicks_nb, distance, keyPressed_nb, session_time
    res = loaded_model.predict(np.array([clicks_nb, distance, keyPressed_nb, session_time]).reshape(1,4))

    if res[0] == 0:
        return False
    else:
        return True
